<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use DB;

class ColumnMapping extends Model
{
	protected $table = 'column_mapping';

	public $timestamps = false;

	public function getQueryColumns() {
		return DB::select("SELECT resource, class, GROUP_CONCAT(system_name SEPARATOR ',') as columns FROM `column_mapping` GROUP BY resource, class ");
	}

	public function getMapping(){
		return DB::select("SELECT column_name, system_name from column_mapping");
	}
}