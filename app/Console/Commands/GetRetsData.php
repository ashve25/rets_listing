<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ColumnMapping;
use App\Listing;

class GetRetsData extends Command
{

    private $rets;
    private $records = array();

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rets:properties {--listing_date=} {--limit=} {--offset=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches Properties Data from RETS Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $config = new \PHRETS\Configuration;
        $config->setLoginUrl(env('RETS_LOGIN_URL'));
        $config->setUsername(env('RETS_USERNAME'));
        $config->setPassword(env('RETS_PASSWORD'));

        // optional.  value shown below are the defaults used when not overridden
        $config->setRetsVersion('1.5'); // see constants from \PHRETS\Versions\RETSVersion
        $config->setUserAgent('RETSMD/1.0');
        $config->setUserAgentPassword(""); // string password, if given
        $config->setHttpAuthenticationMethod('digest'); // or 'basic' if required 
        
        $this->rets = new \PHRETS\Session($config);
        $bulletin = $this->rets->Login();
       
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $columnMapping = new ColumnMapping;
        $map = $columnMapping->getQueryColumns();
        $columnToSystemMap = collect($columnMapping->getMapping());

        
        $keyed = $columnToSystemMap->mapWithKeys(function ($item) {
            $item = (array) $item;
            return [$item['system_name'] => $item['column_name']];
        });
        
        foreach($map as $value) {
            $this->callRETS($value->resource, $value->class, $value->columns);
        }
        \Log::info($keyed);
        foreach($this->records as $record) {
            $listing = new Listing;
            foreach ($record->getFields() as $key => $val) {
                \Log::info($val);
                $columnName = $keyed[$val];
                $value = $record->get($val);
                if(empty($value)) $value = null;
                $listing->{$columnName} = $value;
            }
            $listing->save();
        }
    }


    private function callRETS($resource, $class, $toSelect ) {
        $columnMapping = new ColumnMapping;
        $columnToSystemMap = (array)$columnMapping->getMapping();
        
        $option = $this->option('listing_date');
        $limit = $this->option('limit');
        $offset = $this->option('offset');
        
        $search = $this->rets->search($resource ,$class, "(ListDate=$option+)",
        array( "Count" => 1, "Select"=>$toSelect, "Format" => "COMPACT", "Limit" => $limit, "Offset" => $offset, "StandardNames" => 0)
        );
        $data = array();
        foreach ($search as $record) {
            array_push($this->records, $record);
        }
    }
}
