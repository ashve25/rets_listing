-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 29, 2017 at 01:02 PM
-- Server version: 5.7.17
-- PHP Version: 7.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rets`
--

-- --------------------------------------------------------

--
-- Table structure for table `column_mapping`
--

CREATE TABLE `column_mapping` (
  `column_name` varchar(500) DEFAULT NULL,
  `resource` varchar(500) DEFAULT NULL,
  `class` varchar(500) DEFAULT NULL,
  `system_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `column_mapping`
--

INSERT INTO `column_mapping` (`column_name`, `resource`, `class`, `system_name`) VALUES
('listing_id', 'Property', 'ALL', 'ListingID'),
('address_street', 'Property', 'ALL', 'FullStreetAddress'),
('city', 'Property', 'ALL', 'CityName'),
('state', 'Property', 'ALL', 'State'),
('zip', 'Property', 'ALL', 'PostalCode'),
('status', 'Property', 'ALL', 'ListingStatus'),
('year_built', 'Property', 'ALL', 'YearBuilt'),
('subdivision', 'Property', 'ALL', 'Subdivision'),
('list_office', 'Property', 'ALL', 'ListOfficeName'),
('original_price', 'Property', 'ALL', 'OriginalListPrice'),
('off_market_date', 'Property', 'LOT', 'OffMarketDate'),
('elem_school', 'Property', 'ALL', 'ElementarySchool'),
('high_school', 'Property', 'ALL', 'HighSchool'),
('middle_school', 'Property', 'ALL', 'MiddleSchool'),
('latitude', 'Property', 'ALL', 'Latitude'),
('longitude', 'Property', 'ALL', 'Longitude'),
('owner_phone', 'Property', 'ALL', 'OwnerOfficePhone'),
('fireplaces', 'Property', 'MF', 'Fireplaces'),
('baths_full', 'Property', 'ALL', 'BathsFull'),
('baths_half', 'Property', 'ALL', 'BathsHalf'),
('bedrooms', 'Property', 'ALL', 'Beds'),
('total_finished_area', 'Property', 'ALL', 'LivingArea');

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE `listings` (
  `id` int(10) UNSIGNED NOT NULL,
  `listing_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_id` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_price` int(11) DEFAULT NULL,
  `asking_price` int(11) DEFAULT NULL,
  `address_number` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_street` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale_rent` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acres` varchar(19) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grid` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_built` int(11) DEFAULT NULL,
  `subdivision` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_agent` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_office` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_price` int(11) DEFAULT NULL,
  `how_sold` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closing_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sold_price` int(11) DEFAULT NULL,
  `selling_agent` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selling_office` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `picture_count` int(11) DEFAULT NULL,
  `off_market_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_id` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_full` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `days_on_market` int(11) DEFAULT NULL,
  `days_on_mls` int(11) DEFAULT NULL,
  `price_per_sqft` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `construction_status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `county` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elem_school` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `high_school` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_school` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_dimensions` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fireplaces` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `baths_full` int(11) DEFAULT NULL,
  `baths_half` int(11) DEFAULT NULL,
  `bedrooms` int(11) DEFAULT NULL,
  `total_finished_area` int(11) DEFAULT NULL,
  `block` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `land_lot` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_to_market_date` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_database_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_property_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amenities` varchar(550) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_amenities` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `basement` varchar(650) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boathouse_dock` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `construction` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cooling_source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cooling_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exterior` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fireplace_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fireplace_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interior` varchar(650) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kitchen_equipment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_description` varchar(550) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lot_size` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `property_setting` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rooms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showing_instructions` varchar(550) COLLATE utf8_unicode_ci DEFAULT NULL,
  `special_conditions` varchar(550) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stories` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `style` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lakewaterfront` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lakechainname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `water_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waterfront_footage` int(11) DEFAULT NULL,
  `waterfrontview` varchar(650) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waterfront_road` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waterfront_present` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waterfront_num` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waterfront_elevation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waterfront_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `directions` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_remarks` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `private_remarks` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_remarks` varchar(1200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idx_include` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`id`, `listing_name`, `listing_slug`, `listing_id`, `uid`, `class`, `area`, `system_price`, `asking_price`, `address_number`, `address_street`, `address`, `city`, `state`, `zip`, `status`, `sale_rent`, `acres`, `grid`, `year_built`, `subdivision`, `list_agent`, `list_office`, `original_price`, `how_sold`, `contract_date`, `closing_date`, `sold_price`, `selling_agent`, `selling_office`, `status_date`, `price_date`, `input_date`, `update_date`, `modified`, `picture_count`, `off_market_date`, `display_id`, `address_full`, `days_on_market`, `days_on_mls`, `price_per_sqft`, `construction_status`, `county`, `elem_school`, `high_school`, `middle_school`, `lot_dimensions`, `latitude`, `longitude`, `owner_phone`, `fireplaces`, `baths_full`, `baths_half`, `bedrooms`, `total_finished_area`, `block`, `district`, `land_lot`, `lot`, `return_to_market_date`, `tax_database_id`, `tax_property_id`, `amenities`, `external_amenities`, `basement`, `boathouse_dock`, `construction`, `cooling_source`, `cooling_type`, `equipment`, `exterior`, `fireplace_location`, `fireplace_type`, `interior`, `kitchen_equipment`, `lot_description`, `lot_size`, `parking`, `property_setting`, `rooms`, `showing_instructions`, `special_conditions`, `stories`, `style`, `lakewaterfront`, `lakechainname`, `water_description`, `waterfront_footage`, `waterfrontview`, `waterfront_road`, `waterfront_present`, `waterfront_num`, `waterfront_elevation`, `waterfront_name`, `directions`, `office_remarks`, `private_remarks`, `public_remarks`, `idx_include`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'CS683891C', NULL, NULL, NULL, NULL, NULL, NULL, '1243 CEDARS CT #C-16', NULL, 'CHARLOTTESVILLE', '10000003034', '22903', '10000069145', NULL, NULL, NULL, 1984, 'CEDARS CRT CONDO', NULL, 'Coldwell Banker Stevens, REALTORS', 40000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '38.05716', '-78.50152', NULL, NULL, 1, NULL, NULL, 390, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(2, NULL, NULL, 'CS616072C', NULL, NULL, NULL, NULL, NULL, NULL, '205 PALATINE AVE. ', NULL, 'CHARLOTTESVILLE', '10000003034', '22902', '10000069143', NULL, NULL, NULL, 1948, 'BELMONT AREA', NULL, 'Century 21 Mid-Virginia Properties', 59900, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '38.01594', '-78.48044', NULL, NULL, 1, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(3, NULL, NULL, 'CS609561C', NULL, NULL, NULL, NULL, NULL, NULL, '2309 HIGHLAND AVE. ', NULL, 'CHARLOTTESVILLE', '10000003034', '22903', '10000069143', NULL, NULL, NULL, 1926, 'DB 163 PG 217', NULL, 'Jefferson Land & Realty, Inc.', 92000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '38.02369', '-78.51141', NULL, NULL, 1, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(4, NULL, NULL, 'GA4397680', NULL, NULL, NULL, NULL, NULL, NULL, 'DEERE RD', NULL, 'ACCIDENT', '10000003008', '21520', '10000069143', NULL, NULL, NULL, NULL, 'TOWN HILL', NULL, 'Custer Realty & Auction', 650000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39.64559', '-79.3695', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(5, NULL, NULL, 'GA4398101', NULL, NULL, NULL, NULL, NULL, NULL, 'MALLARD LAKE TRAIL', NULL, 'SWANTON', '10000003008', '21561', '10000069143', NULL, NULL, NULL, NULL, 'BRANT RD, DCL STATE PARK', NULL, 'Long & Foster Real Estate, Inc.', 32000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39.51517', '-79.29285', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(6, NULL, NULL, 'GA4399818', NULL, NULL, NULL, NULL, NULL, NULL, '38 ANTELOPE DRIVE', NULL, 'OAKLAND', '10000003008', '21550', '10000069143', NULL, NULL, NULL, NULL, 'PINEY MOUNTAIN CORP', NULL, 'Custer Realty & Auction', 4500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39.52583', '-79.45437', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(7, NULL, NULL, 'GA4399821', NULL, NULL, NULL, NULL, NULL, NULL, '18 BEAR DRIVE', NULL, 'OAKLAND', '10000003008', '21550', '10000069143', NULL, NULL, NULL, NULL, 'PINEY MOUNTAIN CORP', NULL, 'Custer Realty & Auction', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39.53041', '-79.437314628', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(8, NULL, NULL, 'GA4400418', NULL, NULL, NULL, NULL, NULL, NULL, '2436 FAIRVIEW RD', NULL, 'GRANTSVILLE', '10000003008', '21536', '10000069143', NULL, NULL, NULL, 1989, 'GRANTSVILLE', NULL, 'Coldwell Banker Deep Creek Realty', 330000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39.59446', '-79.13729', NULL, NULL, 4, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(9, NULL, NULL, 'GA4400557', NULL, NULL, NULL, NULL, NULL, NULL, '2855 AMISH RD', NULL, 'GRANTSVILLE', '10000003008', '21536', '10000069143', NULL, NULL, NULL, 1999, 'GRANTSVILLE', NULL, 'Robison-Skidmore', 72900, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39.66705', '-79.22168', NULL, NULL, 2, NULL, 3, 1152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(10, NULL, NULL, 'GA4400626', NULL, NULL, NULL, NULL, NULL, NULL, 'HIGHLINE DR', NULL, 'MC HENRY', '10000003008', '21541', '10000069143', NULL, NULL, NULL, NULL, 'DEEP CREEK MTN. RESORT', NULL, 'Long & Foster Real Estate, Inc.', 129900, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ACCIDENT', 'NORTHERN GARRETT HIGH', 'NORTHERN', NULL, '39.54399', '-79.36457', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17'),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 07:31:17', '2017-11-29 07:31:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `listings`
--
ALTER TABLE `listings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_status_class` (`status`,`class`),
  ADD KEY `index_status_bedrooms` (`status`,`bedrooms`),
  ADD KEY `index_status_baths_full` (`status`,`baths_full`),
  ADD KEY `index_status_input_date` (`status`,`input_date`),
  ADD KEY `index_status_system_price` (`status`,`system_price`),
  ADD KEY `index_status_picture_count` (`status`,`picture_count`),
  ADD KEY `index_listing_id_zip` (`listing_id`,`zip`),
  ADD KEY `index_listing_id_city` (`listing_id`,`city`),
  ADD KEY `index_listing_id_status` (`listing_id`,`status`),
  ADD KEY `index_listing_id_county` (`listing_id`,`county`),
  ADD KEY `index_listing_id_subdivision` (`listing_id`,`subdivision`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `listings`
--
ALTER TABLE `listings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
